Ce thème est un thème Omeka Classic personnalisé pour le rojet Fonte Gaia.

# Installation
* Comme n'importe quel thème Omeka. Attention à renommer le dossier destination en z-seasons

Pour installer ce thème en tant que sous-module git, utiliser la commande suivante : `git submodule add https://gitlab.com/litt-arts-num/fonte-gaia/projet-theme.git themes/z-seasons`