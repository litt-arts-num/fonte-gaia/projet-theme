��            )         �     �  
   �     �     �     �            
        #     0     8  
   =      H     i     }     �     �     �     �     �  !   �     �     �            	   #     -     B     O     V  L  s     �     �     �     �  (        =     T  
   Z     e  	   s     }     �  '   �     �     �     �                     7  '   ?     g  #   o  '   �     �     �     �     �  	   �  $                                              	                                                                      
                       Add a Field Browse All Browse Collections Browse Exhibits Browse Exhibits by Tag Browse Items Citation Collection Contributors Creator Date Date Added Discover our digital collections Featured Collection Featured Item Featured/Non-Featured Files First Items in the %s Collection Last More about the Fonte Gaia project Next Only Featured Items Only Public Items Previous Publisher Recently Added Items Remove field Search Visit our contributor's blog Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-11 16:19+0200
Last-Translator: Camille <camille.desiles@univ-grenoble-alpes.fr>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter un champ Tout parcourir Parcourir les collections Parcourir les expositions Parcourir les expositions par mots-clefs Parcourir les contenus Citer Collection Contributeurs Créateur Année Date ajoutée Découvrez nos collections numérisées Collection mise en avant Contenu mis en avant Mis en avant/Non mis en avant Fichier Premier Les contenus de la %s collection Dernier En savoir plus sur le projet Fonte Gaia Suivant Seulement les contenus mis en avant Seulement les contenus non mis en avant Précédent Éditeur Contenu récemment ajouté Champ supprimé Recherche Visitez le blog de nos contributeurs 