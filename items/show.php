<?php echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'items show')); ?>
    <?php /*echo all_element_texts('item'); */?>

 <?php /*   SN
    <?php if ((get_theme_option('Item FileGallery') == 0) && metadata('item', 'has files')): ?>
    <?php echo files_for_item(array('imageSize' => 'fullsize')); ?>
    <?php endif; ?>
*/ ?>


<div id="primary">
    <!-- The following returns all of the files associated with an item. -->
    <?php if ((get_theme_option('Item FileGallery') == 1) && metadata('item', 'has files')): ?>
    <div id="itemfiles" class="element">
        <h2><?php echo __('Files'); ?></h2>
        <?php echo item_image_gallery(); ?>
    </div>
    <?php endif; ?>

    <!-- The following prints a list of all tags associated with the item -->
    <?php if (metadata('item', 'has tags')): ?>
    <div id="item-tags" class="element">
        <h2><?php echo __('Tags'); ?></h2>
        <div class="element-text"><?php echo tag_string('item'); ?></div>
    </div>
    <?php endif;?>

    <?php if ($description = metadata('item', array('Dublin Core', 'Title'), array('snippet'=>250))): ?>
    <div class="item-description">
        <h2 id="main-title"><?php echo $description; ?></h2>
    </div>
    <?php endif; ?>

    <?php if (get_theme_option('Item Filter Metadata') == 1): ?>

<!--
      <?php if ($description = metadata('item', array('Dublin Core', 'Subject'), array('snippet'=>250))): ?>
      <div class="item-description">
          <?php echo "<b>".__('Subject')."</b> : &nbsp;&nbsp;&nbsp;&nbsp;" .$description; ?>
      </div>
      <?php endif; ?>
-->

      <?php if ($description = metadata('item', array('Dublin Core', 'Creator'), array('snippet'=>250))): ?>
      <div class="item-description">
          <?php echo "<b>" .__('Creator')."</b> : ".$description; ?>
      </div>
      <?php endif; ?>

      <?php if ($description = metadata('item', array('Dublin Core', 'Publisher'), array('snippet'=>250))): ?>
      <div class="item-description">
          <?php echo "<b>" .__('Publisher')."</b> : ".$description; ?>
      </div>
      <?php endif; ?>

      <?php if ($description = metadata('item', array('Dublin Core', 'Date'), array('snippet'=>250))): ?>
      <div class="item-description">
          <?php echo "<b>" .__('Date')."</b> : ".$description; ?>
      </div>
      <?php endif; ?>

      <!-- If the item belongs to a collection, the following creates a link to that collection. -->
      <?php if (metadata('item', 'Collection Name')): ?>
      <div class="item-description">
        <b><?php echo __('Collection'); ?></b> : <?php echo link_to_collection_for_item(); ?>
      </div>
      <?php endif; ?>
    <?php else: ?>
      <?php // echo all_element_texts('item'); ?>
    <?php endif; ?>

    <!-- The following prints a citation for this item. -->
<!--
    <div class="item-description">
        <b><?php echo __('Citation'); ?></b> : <?php echo metadata('item', 'citation', array('no_escape' => true)); ?>
    </div>
-->
</div><!-- end primary -->


<aside id="sidebar">
  <div id="contribution">
    <a href="<?php echo WEB_ROOT ?>/contribution">Soumettre un contenu</a>
  </div>
  <?php $corrections = get_specific_plugin_hook_output('Corrections', 'public_items_show', array('view' => $view, 'item' => $item));
    if ($corrections) : ?>
      <div id="corrections">
        <?php echo $corrections; ?>
        </div>
<?php endif; ?>
<!--
    <div class="item-img">
        <?php echo item_image('thumbnail'); ?>
    </div>
-->
</aside>

<div class="item hentry visionneuse">
  <?php
    echo get_specific_plugin_hook_output('UniversalViewer', 'public_items_show', array('view' => $view, 'item' => $item));
//     fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item));
  ?>
</div>
<div id="comments">
   <?php echo get_specific_plugin_hook_output('Commenting', 'public_items_show', array('view' => $view, 'item' => $item)); ?>
</div>

<ul class="item-pagination navigation">
    <li id="previous-item" class="previous"><?php echo link_to_previous_item_show(); ?></li>
    <li id="next-item" class="next"><?php echo link_to_next_item_show(); ?></li>
</ul>

<?php echo foot(); ?>
