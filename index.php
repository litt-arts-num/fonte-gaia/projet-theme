<?php echo head(array('bodyid'=>'home')); ?>
<?php $theme_path = substr(dirname(__FILE__), 14); ?>

<?php if (get_theme_option('Homepage Text')): ?>
<p><?php echo get_theme_option('Homepage Text'); ?></p>
<?php endif; ?>

<div id="centre-hexag">
	<div id="hex1" class=" hexag fond-blanc">
		<div class="hex_gauche"></div>
		<div class="hex poly1">
			<div class="hex_contenu">
              <a href="<?php echo get_theme_option('Homepage Second Link Link'); ?>"><?php echo t(get_theme_option('Homepage Second Link Text')  ); ?></a>
            </div>
		</div>
		<div class="hex_droit"></div>
	</div>
	<div id="hex2" class="hexag fond-noir hexag_img">
		<img src="<?php echo $theme_path ?>/css/hexag-01.png" width="250px">
	</div>
<!--	<div id="hex2" class="hexag fond-noir">
		<div class="hex_gauche"></div>
		<div class="hex poly1">
			<div class="hex_contenu"><?php echo __('Suivre l\'actualité du projet Gaia'); ?></div>
		</div>
		<div class="hex_droit"></div>
	</div>
-->
	<div id="hex3" class="hexag fond-noir">
		<div class="hex_gauche"></div>
		<div class="hex poly1">
          <div class="hex_contenu">
            <a href="<?php echo get_theme_option('Homepage First Link Link'); ?>"><?php echo t(get_theme_option('Homepage First Link Text')); ?></a>
          </div>
		</div>
		<div class="hex_droit"></div>
	</div>
	<div id="hex4" class=" hexag fond-blanc">
		<div class="hex_gauche"></div>
		<div class="hex poly1">
          <div class="hex_contenu">
            <a href="<?php echo get_theme_option('Homepage Third Link Link'); ?>"><?php echo t(get_theme_option('Homepage Third Link Text')); ?></a>
          </div>
		</div>
		<div class="hex_droit"></div>
	</div>
	<div id="hex5" class="hexag_img">
		<img src="<?php echo $theme_path ?>/css/hexag-03.png" width="160px">
	</div>
	<div id="hex6" class="hexag_img">
		<img src="<?php echo $theme_path ?>/css/hexag-04.png" width="160px">
	</div>

</div> <!-- centre-hexag -->


<?php if (get_theme_option('Display Featured Item') !== '0'): ?>
<!-- Featured Item -->
<div id="featured-item">
    <h2><?php echo __('Featured Item'); ?></h2>
    <?php echo random_featured_items(1); ?>
</div><!--end featured-item-->
<?php endif; ?>

<?php fire_plugin_hook('public_content_top', array('view'=>$this)); ?>

<?php if (get_theme_option('Display Featured Collection') !== '0'): ?>
<!-- Featured Collection -->
<div id="featured-collection">
    <h2><?php echo __('Featured Collection'); ?></h2>
    <?php echo random_featured_collection(); ?>
</div><!-- end featured collection -->
<?php endif; ?>

<?php if ((get_theme_option('Display Featured Exhibit') !== '0')
        && plugin_is_active('ExhibitBuilder')
        && function_exists('exhibit_builder_display_random_featured_exhibit')): ?>
<!-- Featured Exhibit -->
<?php echo exhibit_builder_display_random_featured_exhibit(); ?>
<?php endif; ?>

<?php
$recentItems = get_theme_option('Homepage Recent Items');
if ($recentItems === null || $recentItems === ''):
    $recentItems = 3;
else:
    $recentItems = (int) $recentItems;
endif;
if ($recentItems):
?>
<div id="recent-items">
    <h2><?php echo __('Recently Added Items'); ?></h2>
    <?php echo recent_items($recentItems); ?>
    <p class="view-items-link"><a href="<?php echo html_escape(url('items')); ?>"><?php echo __('View All Items'); ?></a></p>
</div><!--end recent-items -->
<?php endif; ?>

<?php fire_plugin_hook('public_home', array('view' => $this)); ?>

<?php echo foot(); ?>
